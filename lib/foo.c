#include "foo.h"

int foo_add(int n, int *pInts)
{
  int total = 0;

  while(n)
  {
    total += *pInts;
    --n;
    ++pInts;
  }

  return total;
}
