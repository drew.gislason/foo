# foo README.md

foo v2.0

Adds multiple integers.

## Changes for v2.0

* Breaks compatibility with foo 1.x
* Removed old foo_add()
* Renamed foo_add_ex() to foo_add()

## Functions

int foo_add(int n, int *pInts);
